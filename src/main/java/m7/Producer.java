package m7;


import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Producer {
    public static final String EXCHANGE = "topic_exhage";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(EXCHANGE, BuiltinExchangeType.TOPIC, true);
        channel.basicPublish(EXCHANGE, "sms.aa", null, ("sms.bbbbbbbbb").getBytes(StandardCharsets.UTF_8));
        channel.close();
    }
}
