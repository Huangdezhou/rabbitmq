package fore;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import two.RabbitmqUtils;

public class Work2 {
    public static final String exchage_name = "direct_logs";
    public static final String queue = "disk";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(exchage_name, BuiltinExchangeType.DIRECT);
        channel.queueDeclare(queue, false, false, false, null);
        channel.queueBind(queue, exchage_name, "error");

        DeliverCallback deliverCallback = (consumerTag, msg) -> {
            System.out.println(new String(msg.getBody()));
        };
        channel.basicConsume(queue, true, deliverCallback, (CancelCallback) null);
    }
}
