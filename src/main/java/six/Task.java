package six;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import two.RabbitmqUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Task {

    public static void main(String[] args) throws IOException, InterruptedException {
        Channel channel = RabbitmqUtils.getChannel();
        String hel233 = "hel233";
        channel.queueDeclare(hel233, true, false, false, null);
        channel.confirmSelect();
        channel.addConfirmListener(new ConfirmListener() {
            @Override
            public void handleAck(long deliveryTag, boolean multiple) throws IOException {
            }
            @Override
            public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                System.out.print("NNack:" + deliveryTag);
            }
        });
        for (int i = 1; i <= 800000; i++) {
            Thread.sleep(50);
            channel.basicPublish("", hel233, null, new String(i + "").getBytes(StandardCharsets.UTF_8)
            );
        }



    }


}
