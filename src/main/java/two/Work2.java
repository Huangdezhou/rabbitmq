package two;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

public class Work2 {
    public static final String queue_name = "hello2";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("wro02" + new String(message.getBody()));
            try {
                Thread.sleep(4999);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        };
        channel.basicQos(1);
        System.out.println("c2.。。。。。");
        channel.basicConsume(queue_name, false, deliverCallback, (CancelCallback) null);
    }
}
