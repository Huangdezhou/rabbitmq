package two;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

public class Work1 {
    public static final String queue_name = "hello2";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("wro01" + new String(message.getBody()));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        };
        Thread.sleep(2000);
        channel.basicQos(1);
        System.out.println("c1.。。。。。");
        channel.basicConsume(queue_name, false, deliverCallback, (CancelCallback) null);
    }
}
