package two;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.ConcurrentSkipListMap;

public class Task {
    public static final String queue_name = "confirm";
    public static final Integer queue_count = 1000;

    public static void main(String[] args) throws IOException {

        simple3();

    }

    private static void simple() throws IOException {
        Channel channel = RabbitmqUtils.getChannel();
        channel.confirmSelect();
        channel.queueDeclare(queue_name, true, false, false, null);
        Long bein = System.currentTimeMillis();
        for (int i = 0; i < queue_count; i++) {

            channel.basicPublish("", queue_name, MessageProperties.PERSISTENT_TEXT_PLAIN, new String(i + "").getBytes(StandardCharsets.UTF_8)
            );
            try {
                boolean waitForConfirms = channel.waitForConfirms();
                if (waitForConfirms) System.out.println("消息发布成功");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - bein) + "ms");
    }

    private static void simple2() throws IOException {
        Channel channel = RabbitmqUtils.getChannel();
        channel.confirmSelect();
        channel.queueDeclare(queue_name, true, false, false, null);
        Long bein = System.currentTimeMillis();
        int batch_count = 100;
        for (int i = 0; i < queue_count; i++) {

            channel.basicPublish("", queue_name, MessageProperties.PERSISTENT_TEXT_PLAIN, new String(i + "").getBytes(StandardCharsets.UTF_8)
            );
            try {
                if (i % batch_count == 0) {
                    boolean waitForConfirms = channel.waitForConfirms();
                    if (waitForConfirms) System.out.println("消息发布成功");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - bein) + "ms");
    }

    private static void simple3() throws IOException {
        Channel channel = RabbitmqUtils.getChannel();
        channel.confirmSelect();
        ConcurrentSkipListMap map = new ConcurrentSkipListMap();
        channel.queueDeclare(queue_name, false, false, false, null);

        ConfirmCallback ackCallback = (deliveryTag, multiple) -> {
            map.remove(deliveryTag);
            System.out.println("确认消息：" + deliveryTag);
        };
        ConfirmCallback nackCallback = (deliveryTag, multiple) -> {
            System.out.println("位确认消息：" + deliveryTag);
        };
        channel.addConfirmListener(ackCallback, nackCallback);
        Long bein = System.currentTimeMillis();

        for (int i = 0; i < queue_count; i++) {
            channel.basicPublish("", queue_name, MessageProperties.PERSISTENT_TEXT_PLAIN, new String(i + "").getBytes(StandardCharsets.UTF_8)
            );
            map.put(channel.getNextPublishSeqNo(), i + "");
        }
        Long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - bein) + "ms");
    }
}
