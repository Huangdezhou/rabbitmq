package my3;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Producer {
    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        String type = "routing";
        channel.exchangeDeclare(type, BuiltinExchangeType.DIRECT,true,false,false,null);
        String qu1 = "routing_q1";
        String qu2 = "routing_q2";
        channel.queueDeclare(qu1,true,false,false,null);
        channel.queueDeclare(qu2,true,false,false,null);
        channel.queueBind(qu1,type,"error");

        channel.queueBind(qu2,type,"info");
        channel.queueBind(qu2,type,"warm");
        channel.queueBind(qu2,type,"error");

        channel.basicPublish(type,"warm",null,"warm信息：：xxx".getBytes(StandardCharsets.UTF_8));
        channel.basicPublish(type,"info",null,"info日志信息：：xxx".getBytes(StandardCharsets.UTF_8));
        channel.basicPublish(type,"error",null,"error日志信息：：xxx".getBytes(StandardCharsets.UTF_8));
    }
}
