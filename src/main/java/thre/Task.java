package thre;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;
import com.rabbitmq.client.MessageProperties;
import two.RabbitmqUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.ConcurrentSkipListMap;

public class Task {
    public static final String exchage_name = "logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(exchage_name, "fanout");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            channel.basicPublish(exchage_name, "", null, new String(next).getBytes(StandardCharsets.UTF_8)
            );
            System.out.println("next");
        }
    }


}
