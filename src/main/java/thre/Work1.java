package thre;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DeliverCallback;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Work1 {
    public static final String exchage_name = "logs";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(exchage_name, "fanout");
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue, exchage_name, "");

        DeliverCallback deliverCallback=(consumerTag,msg)->{
            System.out.println(new String(msg.getBody()));
        } ;
        channel.basicConsume(queue,true,deliverCallback, (CancelCallback) null);
    }
}
