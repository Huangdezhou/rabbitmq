package five;

import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Task {
    public static final String exchage_name = "topic_logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtils.getChannel();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            channel.basicPublish(exchage_name, "test.origin.rabbit", null, new String(next).getBytes(StandardCharsets.UTF_8)
            );
            System.out.println("next");
        }
    }


}
