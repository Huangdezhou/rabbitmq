package five;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import two.RabbitmqUtils;

public class Work1 {
    public static final String exchage_name = "topic_logs";
    public static final String queue = "q1";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(exchage_name, BuiltinExchangeType.TOPIC);
        channel.queueDeclare(queue, false, false, false, null);
        channel.queueBind(queue, exchage_name, "*.origin.*");

        DeliverCallback deliverCallback = (consumerTag, msg) -> {
            System.out.println(new String(msg.getBody()));
        };
        channel.basicConsume(queue, true, deliverCallback, (CancelCallback) null);
    }
}
