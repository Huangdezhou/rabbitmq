package my2;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Producer {
    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        String type = "fanout";
        channel.exchangeDeclare(type, BuiltinExchangeType.FANOUT,true,false,false,null);
        String qu1 = "fanout_q1";
        String qu2 = "fanout_q2";
        channel.queueDeclare(qu1,true,false,false,null);
        channel.queueDeclare(qu2,true,false,false,null);
        channel.queueBind(qu1,type,"");
        channel.queueBind(qu2,type,"");

        channel.basicPublish(type,"",null,"日志信息：：xxx".getBytes(StandardCharsets.UTF_8));
    }
}
