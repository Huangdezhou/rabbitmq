package my6;


import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Producer {
    public static final String EXCHANGE = "direct_exhage";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(EXCHANGE, BuiltinExchangeType.DIRECT, true);
        channel.basicPublish(EXCHANGE, "email", null, ("email").getBytes(StandardCharsets.UTF_8));
        channel.close();
    }
}
