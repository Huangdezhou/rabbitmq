package my1;


import com.rabbitmq.client.*;
import two.RabbitmqUtils;

import java.io.IOException;

public class Comsumer2 {
    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        Consumer com=new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("c2:body:"+new String(body));
            }
        };
        channel.basicConsume("workqueue",true,com);
    }
}
