package my1;


import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Producer {
    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.queueDeclare("workqueue", false, false, false, null);
        for (int i = 0; i < 11; i++) {
            channel.basicPublish("", "workqueue", null, ("hell:" + i).getBytes(StandardCharsets.UTF_8));
        }
        channel.close();
    }
}
