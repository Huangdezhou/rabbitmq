package my4;


import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import two.RabbitmqUtils;

import java.nio.charset.StandardCharsets;

public class Producer {
    public static final String EXCHANGE = "fanout_exhage";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(EXCHANGE, BuiltinExchangeType.FANOUT, true);
        channel.basicPublish(EXCHANGE, "", null, ("ddddddddddb").getBytes(StandardCharsets.UTF_8));
        channel.close();
    }
}
