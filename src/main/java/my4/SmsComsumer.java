package my4;

import com.rabbitmq.client.*;
import two.RabbitmqUtils;

import java.io.IOException;

public class SmsComsumer {
    public static final String SMS_QUEUE = "SMS_QUEUE";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.queueDeclare(SMS_QUEUE, true, false, false, null);
        channel.queueBind(SMS_QUEUE, "fanout_exhage", "");
        Consumer com = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bod2y = new String(body);
                System.out.println(bod2y);
                channel.basicAck(envelope.getDeliveryTag(), false);

            }
        };
        channel.basicConsume(SMS_QUEUE, false, com);
    }
}
